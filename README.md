# Machine learning Multi-label Text Classification
In these notebooks, I will develop text classification models. For this, I will analyze a textual synopsis and predicts multiple labels/genres associated with it. I'll implement a conventional machine learning (LR) and a deep learning algorithm(RNN).

## My goal
Implement a complete ML pipepine with the following:


* Data pre-processing/Cleaning (see  predicting-movie-genres-nlp-with-LR file)
* Web scraping a dataset : To collect informationa bout genre on other platform (see Get_movie_genre_from_Netflix)
* Non-deep Machine Learning models : Logisitc Regresion (see  predicting-movie-genres-nlp-with-LR file)
* Deep Models for Text: RNN (ongoing)

## Technologies used
For the LR part:
* One-hot encoding genre by using sklearn’s MultiLabelBinarizer()
* Converting text inputs into integer by using the sklearn's TF-IDF feature extraction method
* Using a Logistic regresion model


For the RNN part:
* One-hot encoding genre by using sklearn’s MultiLabelBinarizer()
* Converting text inputs into integer by using the Tokenizer module from keras. It divides a sentence into the corresponding list of word and then it convert the words to integers)
* Converting text inputs (in their integer representation) into emdedded vectors by using the GloVe feature extraction method. Word embeddings capture a lot more information about words relationships between them
* Creating a model with one input layer, one embedding layer, one (or more) LSTM layer and one output layer with approximately

